# Spécialité NSI - Première

## Contenu du dépôt

Ce dépôt compile les __cours de première, dispensés au Lycée Clemenceau à Nantes, en spécialité NSI__(Numérique et Sciences Informatiques).

Ces cours sont essentiellement composés de __notebooks__.

Ces notebooks peuvent être :

- un moyen de travailler un nouveau thème (numérotation classique NSI_X_X_....ipynb)
- un moyen d'appliquer les nouvellles notions par des exercices (NSI_X_XE_....ipynb)
- des propositions de correction de ces exercices (NSI_X_XC_....ipynb)
- un moyen d'approfondir un thème, hors programme (NSI_X_XA_....ipynb)
- un "fil rouge", organisé comme un projet guidé, sur le thème "Harry Potter" (NSI_PX_....ipynb))

Ils sont déposés au fur et à mesure de leur élaboration depuis la rentrée 2020 et sont mis à jour au besoin.

Les sujet d'évaluation, à part les projets, ne sont pas mis en ligne.

## Visualisation / modification et exécution des notebooks

Framagit ne permet pas d'utiliser, ni même de pleinement visualiser nativement les notebooks Jupyter.

Pour profiter pleinement des notebooks, deux options semblent intéressantes :

- __cloner ce dépôt__, puis en profiter en local.
- utiliser ce lien __Binder__ pour une utilisation "en ligne" [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/https%3A%2F%2Fframagit.org%2Fanat3000%2Fnsi/HEAD)

## Méthodologie pédagogique

### Travail sur les notebooks

Les élèves sont accompagnés pour travailler ces notebooks. Ils doivent les compléter / annoter afin d'en faire un __support de cours et de révision__.

### En bref !

Chaque début de cours est ponctué d'un (ou plusieurs) __exposés à oral__, chacun effectué par un seul élève.

Cet exposé, appelé __"En Bref !"__, a pour sujet (au choix de l'élève) :

- un point particulier à détailler, sur le dernier thème abordé
- une actualité
- un sujet en marge des thèmes actuels, permettant d'approfondir nos connaissances

Le sujet ne doit pas être trop général, ni résumer une partie importante du thème. Ce doit être un petit point, bien précis, qu'il convient de détailler et d'approfondir.

Cet exposé doit durer entre 3 et 5 minutes et doit s'appuyer sur un support visuel, tel qu'un diaporama.

A la fin de l'exposé, la classe est invitée à questionner l'élève ayant terminé sa prise de parole.

L'évaluation tient compte du fond et de la forme.

## Progression

La progression ne suit pas la numérotation de chapitres.

Voici une progression possible :

| Numéro de chapitre | Nom du fichier |  
|:----------------:|:------------:|
|0| NSI_0_Introduction_aux_notebooks|
|||
|1|NSI_1_1_Ecrire_un_nombre_entier_positif|
|1|NSI_1_2_Conversions_décimal_binaire|
|1|NSI_1_3_Hexadecimal|
|1|NSI_1_4_Comment_calculer_en_binaire|
|1|NSI_1_4E_Conversions_décimal_binaire_hexa|
|1|NSI_1_5_Texte_et_encodage|
|1|NSI_1_5E_Exos_Encodage|
|1|NSI_1_5EC_Exos_Encodage|
|1|NSI_1_6A_Aller_plus_loin_avec_du_texte|
|||
|2|NSI_2_1_Langage_de_programmation_Python|
|2|NSI_2_2_Variables_et_affectations|
|2|NSI_2_2Bonus_Gestion_Memoire_Chaines|
|2|NSI_2_3A_Au_coeur_des_variables_et_affectations|
|2|NSI_2_4_Tests_conditions_et_booléens|
|2|NSI_2_4E_Tests_conditions_et_booléens|
|2|NSI_2_4C_Tests_conditions_et_booléens|
|2|NSI_2_5A_Tests_et_exceptions|
|2|NSI_2_6_Boucle_bornée_Pour|
|2|NSI_2_7_Boucle_non_bornée_Tant_que|
|2|NSI_2_8E_Exercices_sur_les_boucles|
|2|NSI_2_8EC_Exercices_sur_les_boucles|
|||
|3|NSI_3_1_Type_construit_Les_Tuples|
|3|NSI_3_2_Type_construit_Les_Tableaux|
|3|NSI_3_3_Type_construit_Les_Dictionnaires|
|3|NSI_3_4A_Type_construit_Les_Ensembles|
|||
|P|NSI_P1_Le_Choixpeau_magique_Du_CSV_aux_Tables|
|P|NSI_P1C_Le_Choixpeau_magique_Du_CSV_aux_Tables|
|||
|2|NSI_2_9_Les_Fonctions|
|2|NSI_2_10A_Les_Fonctions_avancees|
|2|NSI_2_9E_Les_Fonctions|
|2|NSI_2_9C_Les_Fonctions|
|||
|P|NSI_P2_Le_Choixpeau_magique_Fonction_du_CSV_aux_Tables|
|P|NSI_P2C_Le_Choixpeau_magique_Fonction_du_CSV_aux_Tables|
|||
|4|NSI_4_1_Algorithmique_et_Pseudo_code|
|4|NSI_4_2_Les_tris|
|4|NSI_4_2C_Les_tris|

## Licence

Les documents déposés ici sont placés sous __[licence CC BY-NC-SA 3.0 FR](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)__.

__Ces documents ont été le plus souvent construits à partir de travaux de collègues ayant eu la bonne idée de placer leur travail sous une licence permettant le partage à certaines conditions__.

A la fin de chacun de mes documents, je source les auteurs des documents qui m'ont permis de faire ces cours.

__J'en profite pour remercier chaleureusement tous les collègues partageant leur travail d'une façon ou d'une autre__.

David Landry
